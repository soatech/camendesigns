./This will be a quick guide on how a typical S3 website will be developed and deployed.  We will use carincamenportfolio.com as an example.

# s3 Buckets

Each site will need to have 4 buckets: carincamenportfolio.com, www.carincamenportfoio.com, images.carincamenportfolio.com, staging.carincamenportfolio.com.

* carincamenportfolio.com will redirect to www.carincamenportfolio.com.
* images.carincamenportfolio.com will be the repository for binary assets
* staging.carincamenportfolio.com will be an area we can deploy to while developing changes that aren't ready for production yet.

# Development

To develop on a site locally you will need to clone the project from Bitbucket.


```
#!bash

$ git clone git@bitbucket.org:soatech/carincamenportfolio.git
```

You will notice there is no images folder.  The only thing in the repo are code and text files.  We are using S3 as an artifactory or repository for all the images.  In order to pull down the latest set of images you will need s3cmd installed and configured.  Then run fetch-img.sh from the scripts folder.
```
#!bash

$ s3cmd --configure
$ cd scripts
$ ./fetch-img.sh
```

*Note: You will need AWS access and secret keys in order to do any of this.  Be sure to check with Eric on this before getting started.*

Once you have the images pulled down you can run the site in your browser (via IntelliJ/Webstorm or your own static site server).  You can add, remove, or modify images in the folder while you develop locally.  Once you are happy with your image changes you can commit them up to the repository.  Be sure the add an entry to the change.log file, describing the work you did.

```
#!bash

$ cd scripts
$ ./push-img.sh
```

Now all your local changes are synced up to the s3 repository for other devs to fetch down.
*Note: This doesn't support merge/conflict resolution.  If two people are making changes on this folder at the same time, there is real chance of losing work.  Be sure to communicate well.*

## IntelliJ/Webstorm Integration

If we are using 3rd party libraries, such as Bootstrap, jQuery, etc and you want that assistance in IntelliJ then you need to pull these down and run them locally.  If you are using CDN you can run a code analysis and have IntelliJ download them for you.  Alternatively, you can use Bower or manually install them and have IntelliJ load the folder.

# Deployment

Our HTML and CSS rely on images and other assets being local.  We don't link directly to images.carincamenportfolio.com.  This makes it so we can experiment with development changes without affecting production.  It also allows us to deploy to multiple environments for various levels of testing.

If we want to test our changes out or want to get feedback on our work before going to production we can deploy to Staging.

```
#!bash

$ cd scripts
$ ./deploy-staging.sh
```

Staging wont be setup on CloudFront and so you can see your changes immediately.  It will run a bit slower than production because of this.  If we are happy with how things appear on Staging we can then deploy to production.

```
#!bash

$ cd scripts
$ ./deploy-prod.sh
```

We will also need to invalidate our files in CloudFront in order to see the changes.