#!/bin/bash

#########
# Setup #
#########

# 1. Run s3cmd with the --configure flag
# s3cmd --configure
# 2. Copy AWS credentials as prompted.  Use HTTPS and encryptions as desired.

if [[ ! ${PWD: -7} = "scripts" ]]; then
    echo "Please run from the scripts folder"
    exit
fi

if [ ! -e ../img ]; then
    echo "Please run from scripts folder"
    exit
fi

s3cmd sync --delete-removed ../img/ s3://images.camendesigns.com
