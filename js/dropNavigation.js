$(document).ready(function($){
    var parPosition = [];
    $('.par').each(function() {
        parPosition.push($(this).offset().top);
    });

    $('a').click(function(){
        $('html, body').animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 500);
        return false;
    });

    $('nav .navbar ul li a').click(function () {
        $('nav .navbar ul li a').removeClass('active');
        $(this).addClass('active');
    });

    $(document).scroll(function(){
        var position = $(document).scrollTop(),
            index;
        for (var i=0; i<parPosition.length; i++) {
            if (position <= parPosition[i]) {
                index = i;
                break;
            }

        }
        $('nav .navbar ul li a').removeClass('active');
        $('nav .navbar ul li a:eq('+index+')').addClass('active');
    });

    $('nav .navbar ul li a').click(function () {
        $('nav .navbar ul li a').removeClass('active');
        $(this).addClass('active');
    });
});
