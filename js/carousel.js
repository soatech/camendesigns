// Activate Carousel
$(".galleryCarousel").carousel();

// Enable Carousel Indicators
$(".item").click(function(){
    $(".galleryCarousel").carousel(1);
});

// Enable Carousel Controls
$(".left").click(function(){
    $(".galleryCarousel").carousel("prev");
});
